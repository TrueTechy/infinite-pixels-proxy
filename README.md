# Infinite Pixels Network Proxy and Analyzer

Provides a TCP proxy for the android game Infinite Pixels (https://play.google.com/store/apps/details?id=me.joshbe.infinitepixels) to monitor and analyse the packets sent back and forth between client and server

Requirements:
 Colour
 
Usage: 

Using the following iptables command to redirect traffic from client to proxu, replacing PROXY_IP with server IP

iptables -t nat -A OUTPUT -p tcp --dport 80 -j DNAT --to-destination=PROXY_IP:80

Running the main.py as root (since port 80 is bound to) will start the proxy, connect to the game server and wait for a client to connect

Once a client connects all the data being passed back and forth between server and client will be available to the functions defined in the parser file, which is reloaded for each packet to allow for editing of the parser without restarting the proxy
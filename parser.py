import struct
from colour import Color

colours = {
		1:"Very Dark Purple",
		2:"Purple",
		3:"Red",
		4:"Orange",
		5:"Yellow",
		6:"Light Green",
		7:"Green",
		8:"Dark Green",
		9:"Dark Blue",
		10:"Blue",
		11:"Light Blue",
		12:"Very Light Blue",
		13:"White",
		14:"Light Grey",
		15:"Dark Grey",
	}

def hex_convert(data):
	return bytes(data).hex()

def h_noop(data, packet_id):
	print("Unknown packet: {} {}".format(hex(packet_id),hex_convert(data)))

def h_discard(data, packet_id):
	print("[Discarded]")

def h_connect(data, packet_id):
	player_id_list = struct.unpack("c"*10, data)
	player_id = ""
	for letter in player_id_list: 
		player_id += chr(int.from_bytes(letter, byteorder='little'))

	print("ID:{} trying to connect".format(player_id.strip()))

def h_position(data, packet_id):
	x,y, xvelocity, yvelocity = struct.unpack("ffff", data)
	print("Player Position: {:.0f} / {:.0f}. Moving {} / {} ".format(x,y, xvelocity, yvelocity))

def h_place_tile(data, packet_id):
	x,y = struct.unpack("ii", data[:-4])
	colour_value = struct.unpack("i", data[-4:])[0]
	print("\"{}\" tile placed at {} / {}".format(colours[colour_value], x,y))

def h_delete_tile(data, packet_id):
	x,y = struct.unpack("ii", data)
	print("Tile deleted at {} / {}".format(x,y))

def h_player_info(data, packet_id):
	player_id_list = struct.unpack("c"*10, data[-10:])
	player_id = ""
	for letter in player_id_list: 
		player_id += chr(int.from_bytes(letter, byteorder='little'))
	tile_colour = colours[int.from_bytes(data[-13:-9], byteorder='little')]

	print(" {} (ME) is holding \"{}\" tile".format(player_id, tile_colour))

def h_other_player_position(data, packet_id):
	x,y, xvelocity, yvelocity = struct.unpack("f"*4, data[:16])
	player_id_list = struct.unpack("c"*10, data[-10:])
	player_id = ""
	for letter in player_id_list: 
		player_id += chr(int.from_bytes(letter, byteorder='little'))
	print("{} Position: {:.0f} / {:.0f}; Velocity {} / {} ".format(player_id.strip(), x,y, xvelocity, yvelocity))

def h_other_player_info(data, packet_id):
	player_id_list = struct.unpack("c"*10, data[:10])
	player_id = ""
	for letter in player_id_list: 
		player_id += chr(int.from_bytes(letter, byteorder='little'))

	r,g,b = struct.unpack("<fff", data[10:22])
	player_color = Color(rgb=(r,g,b))
	tile_colour = colours[int(hex_convert(data[23:27]),16)]
	name_length = int.from_bytes(data[26:27], byteorder='little')
	player_name_list = struct.unpack("c"*name_length, data[-(name_length):])
	player_name = ""
	for letter in player_name_list: 
		player_name += chr(int.from_bytes(letter, byteorder='little'))
	print("{} has name \"{}\" of length {} \n    Is holding \"{}\" tile\n    Is {} Colour".format(player_id.strip(), player_name[-(name_length):], name_length, tile_colour, player_color))
handlers = {
	#client
	0x00: h_connect,
	0x03: h_position, #Figure out velocity issue
	0x07: h_place_tile,
	0x08: h_delete_tile,
	0x09: h_player_info, 
	
	#server
	0x04: h_other_player_position, # Velocity to be fixed
	0x0b: h_other_player_info, # finsh
}

def parse(data, origin):
	#if origin == 'Client':
	#	return

	packet_id = struct.unpack("b", data[0:1])[0]

	#print("[{}] -> {}".format(origin, bytes(data).hex()))
	handlers.get(packet_id, h_noop)(data[1:], packet_id)